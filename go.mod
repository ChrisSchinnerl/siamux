module gitlab.com/NebulousLabs/siamux

go 1.13

require (
	github.com/gorilla/websocket v1.4.2
	gitlab.com/NebulousLabs/encoding v0.0.0-20200604091946-456c3dc907fe
	gitlab.com/NebulousLabs/errors v0.0.0-20171229012116-7ead97ef90b8
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/NebulousLabs/go-upnp v0.0.0-20181011194642-3a71999ed0d3
	gitlab.com/NebulousLabs/log v0.0.0-20200529173103-40b250c2d92c
	gitlab.com/NebulousLabs/persist v0.0.0-20200605115618-007e5e23d877
	gitlab.com/NebulousLabs/threadgroup v0.0.0-20200608151952-38921fbef213
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
)
